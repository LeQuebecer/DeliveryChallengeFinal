package com.company;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    private static Scanner scanner = new Scanner(System.in);
    private static ArrayList<Delivery> deliveryList = new ArrayList<>();


    //To fix
    //Error Results from Inputting Wrong Code For Delivery Location
    //Basic Error Catching - Numbers in String, String in numbers
    //Forcing recommit when above errors results
    // X - COMPLETED //Allowing for multiple packages - store information somewhere, possibly ListArray

    public static void main(String[] args) {



        boolean quit = false;
        int choice = 0;
        System.out.println("Welcome to Canada Post Automated Shipping System:");
        System.out.println("Please Select from the Menu Below: ");
        menu();
        while(!quit){

            System.out.print("Enter Choice (4 for Menu): ");
            choice = scanner.nextInt();
            scanner.nextLine();
            switch(choice){
                case 1:
                    quit = true;
                    System.out.println("Goodbye!");
                    break;
                case 2:
                    mailPackage();
                    break;
                case 3:
                    printProvinceCodes();
                    break;
                case 4:
                    menu();
                    break;
            }
            
            System.out.println("Would you like to return to the main menu?");
            System.out.print("1 - Yes / 2 - No: ");
            int yesNo = scanner.nextInt();
            if( yesNo == 2){
                break;
            }

        }

        printDeliveries();

    }

    //Main Functions

    public static void menu(){
        System.out.println("Option 1: Exit\n" +
                           "Option 2: Mail Package\n" +
                           "Option 3: Province Codes\n" +
                           "Option 4: Reprint Menu");
    }

    public static void mailPackage(){
        Delivery newDelivery = new Delivery();
        double packageWeight;
        boolean verified = false;
        String destination;
        System.out.print("Please enter the package destination: ");
        //This needs to select from a potential list
        do{
            while(!scanner.hasNext()){
                //Keep this, but add another while inside, summoning the verification function;
                String input = scanner.next();
                newDelivery.destinationFound(input);
            }
            destination = scanner.next();
            verified = newDelivery.destinationFound(destination);
            if (!verified){
                System.out.println("Invalid entry - Please try again");
                System.out.print("Enter Destination Again:");
            }

        }while (!verified);


        System.out.print("Please enter the package weight: ");

        //Should I split this off to be its own separate method?
        do{
            while(!scanner.hasNextDouble()){
                String input = scanner.next();
                System.out.println("Invalid entry - Please try again");
                System.out.print("Enter Package Weight:");
            }
            packageWeight = scanner.nextDouble();

        }while (packageWeight < 0);

        double fee = newDelivery.parcelFee(packageWeight);




        //Step 3 - Output - Changed
        Delivery additionalDelivery = new Delivery(destination, packageWeight, fee);
        deliveryList.add(additionalDelivery);


//        if(totalPrice > 0){
//            System.out.println("The total price to mail your package is " + totalPrice + " dollars");
//        }


    }

    public static void printProvinceCodes() {
        System.out.println(" Quebec - QC\n" +
                           "Newfoundland - NFLD\n" +
                           " Prince Edward Island - PEI\n" +
                           "Nova Scotia - NS\n" +
                           "New Brunswick - NB\n" +
                           "Ontario - ONT\n" +
                           "Manitoba - MTB\n" +
                           "Saskatchewan - SKT\n" +
                           "Alberta - ALB\n" +
                           "British Columbia - BC\n" +
                          "North West Territories - NWT\n" +
                          "Nunavut - NVT\n" +
                          "Yukon - YK");
    }

    public static void printDeliveries(){
        for(int i = 0; i < deliveryList.size(); i++){
            System.out.println("Delivery One going to " + deliveryList.get(i).getParcelLocale() + " at a weight of  " + deliveryList.get(i).getParcelWeight() + " for a cost of " + deliveryList.get(i).getTotal());
        }
    }

}
