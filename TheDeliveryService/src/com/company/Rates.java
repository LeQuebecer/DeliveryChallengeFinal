package com.company;

import java.util.HashMap;
import java.util.Map;

public class Rates {
    private Map<String, Double> rateLists;


    public Rates() {
        rateLists = new HashMap<>();
        rateLists.put("QC", 1.0);
        rateLists.put("NFLD", 4.0);
        rateLists.put("PEI", 2.5);
        rateLists.put("NS", 2.0);
        rateLists.put("NB", 1.5);
        rateLists.put("ONT", 1.5);
        rateLists.put("MTB", 2.0);
        rateLists.put("SKT", 3.0);
        rateLists.put("ALB", 4.0);
        rateLists.put("BC", 5.0);
        rateLists.put("NWT", 6.0);
        rateLists.put("NVT", 6.0);
        rateLists.put("YK", 6.0);


    }

    public double selectedRate(String destination) {
        //Verify first
        double rate = rateLists.get(destination);
        return rate;
    }

    public boolean returnDestination(String destination){
        if (rateLists.containsKey(destination)){
            return true;
        }

        return false;
    }

    @Override
    public String toString() {
        return "Rates{" +
                "rateLists=" + rateLists +
                '}';
    }
}

