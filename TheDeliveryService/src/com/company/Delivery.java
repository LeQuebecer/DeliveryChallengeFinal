package com.company;

import java.util.ArrayList;

public class Delivery {
    private double baseRate = 12.25;
    private double totalFee;
    private int rate1 = 1;
    private int rate2 = 5;
    private int rate3 = 20;
    private int fee1 = 5;
    private int fee2 = 6;
    private int fee3 = 8;

    private String parcelLocale;
    private double parcelWeight;
    private double total;
    Rates rate = new Rates();

    public Delivery() {
    }

    public Delivery(String parcelLocale, double parcelWeight, double total){
        //Why is this here?
        //Because unless I have the variables stored further up, we run into an issue.
        //My last problem is I would need the total fee, only then can I pass all three into my Array List and get away with it.
        this.parcelLocale = parcelLocale;
        this.parcelWeight = parcelWeight;
        this.total = total;
    }


    // I need what functions?

    //One that converts Parcel Destination into = Amount - This could be turned into a Boolean return, so I could deal with it simpler.
    //Verification should be don here, NOT in the main as it currently is.
    public double parcelDestination(String parcelDestination) {
        if (rate.returnDestination(parcelDestination)){
            double destinationMultilplier = rate.selectedRate(parcelDestination);
            return destinationMultilplier;
        }
        return 0;
    }

    //Find Destination Method
    //Goal - Cycle through Map Key and determine whether the destination is found - returns Boolean True/False to move onto the next step.

    public boolean destinationFound(String parcelDestination){
        if( rate.returnDestination(parcelDestination)){
            return true;
        }
        return false;
    }

    //One that checks ParcelWeight against the options and runs that through to get additional fee.

    public double parcelFee(double parcelWeight) {
        double parcelFee = -1;
        if (parcelWeight > 0 && parcelWeight < rate1) {
            parcelFee = fee1;
        } else if (parcelWeight < rate2) {
            parcelFee = fee2 * parcelWeight;
        } else if (parcelWeight <= rate3) {
            parcelFee = fee3 * parcelWeight;
        }
        return parcelFee;
    }


    //One that tabulates it all together.

    public double deliveryFee(double shippingFee, double destinationMultiplier) {
        if (shippingFee == -1){
            System.out.println("Packaged rejected - Over weight limit");
            return 0;
        }else if (destinationMultiplier == -1){
            System.out.println("Package rejected - Location code incorrect");
            return 0;
        }else {
            totalFee = (baseRate * destinationMultiplier) + (shippingFee);
            return totalFee;
        }
    }

    public String getParcelLocale() {
        return parcelLocale;
    }

    public double getParcelWeight() {
        return parcelWeight;
    }

    public double getTotal() {
        return total;
    }
}
